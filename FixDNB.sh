#!/bin/bash
#
##################################################################################################
#
# NAME
#   FixDNB.sh
#
# DESCRIPTION
#       - Fix DnB usage files
#
# INPUTS
#   (none)
#
# OUTPUTS
#   (none)
#
# REMARKS
#   (none)
#
# ------------------------------------------------------------------------------------------
# DATE:        03-06-2014
# AUTHOR:      Cvidya - Ilyia Stanev (Ilyia.Stanev@cvidya.com)
# DESCRIPTION: version 1.00
#
# DATE:        06-08-2014
# AUTHOR:      PT DOS - Joao Pimentel (joao-a-pimentel@telecom.pt)
# DESCRIPTION: Added output to log file
#              Copy the output file to processed folder
#              Archive original file in archive folder
#
# DATE:        22-06-2020
# AUTHOR:      PT DIT - Fernando Paz (fernando-paz@telecom.pt)
# DESCRIPTION: A compressed copy of the original file is created in
#              a temporary folder so it can be uploaded to DBAs ETF account
#
# DATE:        22-11-2022
# AUTHOR:      DIT - Diamantino Carreira (diamantino-p-carreira@telecom.pt)
# DESCRIPTION: New input layout
#
###################################################################################################
#set -n   # Uncomment to check sintax, without execution.
#set -x   # Uncomment to debug this shell script

#############################
# Global interface
#############################

. /iris/scripts/utils.sh

#############################
# Variaveis Locais
#############################

SCRIPTNAME=$(basename $0)


PATHLOG=${BASELOG}/preprocess_scripts
LOG=${PATHLOG}/log.txt.${year}-${month}-${day}

INPATH='/iris/DATA1/out/dlu/CD/'
DONEPATH='/iris/DATA1/out/dlu/CD/data/'

PROCESSEDPATH='/iris/DATA1/out/dlu/CD/processed/'
ARCHIVEPATH='/iris/DATA1/out/dlu/CD/archive/'
DBAPATH='/iris/DATA1/out/dlu/CD/dbatransfer/tmp/'

TMP1='.tmp1'
TMP2='.tmp2'
TMP3='.tmp3'
TMP4='.tmp4'
TMP5='.tmp5'
TMP6='.tmp6'


#############################
# Main
#############################


cd $INPATH
INFILES=`ls tblMERCADO_*.zip`
NFILES=`find $INPATH -maxdepth 1 -name "tblMERCADO_*.zip" | wc -l`
DONE='.dat'

for FILENAME in $INFILES
do

	cp -p $FILENAME $DBAPATH
	mv $DBAPATH$FILENAME $DBAPATH../
	
	echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Uncompressing file ${FILENAME}" >> ${LOG}
	unzip $FILENAME
	
	BASEFILE='tblMERCADO.txt'
	TMP1FILE=$INPATH$BASEFILE$TMP1
	TMP2FILE=$INPATH$BASEFILE$TMP2
	TMP3FILE=$INPATH$BASEFILE$TMP3
	TMP4FILE=$INPATH$BASEFILE$TMP4
	TMP5FILE=$INPATH$BASEFILE$TMP5
	TMP6FILE=$INPATH$BASEFILE$TMP6

	cat $BASEFILE > $TMP1FILE
	
	iconv -c -f iso-8859-1 -t utf-8 $TMP1FILE > $TMP2FILE

	   
	awk 'BEGIN {FS=";"; OFS=";"} {
		aux0=substr($33,1,length($33)-1)";"
		aux1=";"
		aux2=";;"
		aux4=";;;;"
		aux7=";;;;;;;"
		aux8=";;;;;;;;"
		aux9=";;;;;;;;;"
		aux11=";;;;;;;;;;;"
		aux15=";;;;;;;;;;;;;;;"
		$15=$15aux2
		$18=$18aux1
		$19=$19aux11
		$21=$21aux9
		$22=$22aux4
		$23=$23aux1
		$27=$27aux11
		$29=$29aux8
		$30=$30aux15
		$32=$32aux7
		$33=substr($33,1,length($33)-1)";"
		print $0}' $TMP2FILE > $TMP3FILE
		
	awk 'BEGIN {FS=";"; OFS=";"} {
		$11 = substr($11,0,40)
		print $0}' $TMP3FILE > $TMP4FILE
		
	awk 'BEGIN {FS=";"; OFS=";"} { printf "%s\r\n", $0}' $TMP4FILE > $TMP5FILE

	iconv -c -f utf-8 -t cp1252 $TMP5FILE > $TMP6FILE


	tail -n +2 $TMP6FILE	 | tr -d '\"' > DUNB_F_`date +%Y%d%m%H%M%S`$DONE
	
	for FILE in DUNB_F_*.dat
	do
		cp $FILE $PROCESSEDPATH
		mv $FILE $DONEPATH
		echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Generated file ${FILENAME}: ${FILE} and saved in ${PROCESSEDPATH}" >> ${LOG}
	done
	
	rm -rf tblMERCADO.txt
	rm $TMP1FILE
	rm $TMP2FILE
	rm $TMP3FILE
	rm $TMP4FILE
	rm $TMP5FILE
	rm $TMP6FILE
	
	mv $FILENAME $ARCHIVEPATH
	echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Archived file ${FILENAME} to ${ARCHIVEPATH} " >> ${LOG}
done

if [ $NFILES -eq 0 ]; then
	echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - No files found to process" >> ${LOG}
fi

###END#############################################################################################
