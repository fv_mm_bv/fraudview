#!/bin/sh
#
##################################################################################################
#
# NAME
#   FixDNB.sh
#
# DESCRIPTION
#       - Fix DnBGEST usage files
#
# INPUTS
#   (none)
#
# OUTPUTS
#   (none)
#
# REMARKS
#   (none)
#
# ------------------------------------------------------------------------------------------
# DATE:        10-03-2015
# AUTHOR:      MEO - Diamantino Carreira (diamantino-p-carreira@telecom.pt)
# DESCRIPTION: version 1.00
#
# DATE:        21-11-2022
# AUTHOR:      DIT - Diamantino Carreira (diamantino-p-carreira@telecom.pt)
# DESCRIPTION: New input layout
#
###################################################################################################
#set -n   # Uncomment to check sintax, without execution.
#set -x   # Uncomment to debug this shell script

#############################
# Global interface
#############################

. /iris/scripts/utils.sh

#############################
# Variaveis Locais
#############################
SCRIPTNAME=$(basename $0)


SCRIPTNAME=$(basename $0)


PATHLOG=${BASELOG}/preprocess_scripts
LOG=${PATHLOG}/log.txt.${year}-${month}-${day}

INPATH='/iris/DATA1/out/dlu/CD/'
OUTPATH='/iris/DATA1/out/dlu/CD/data/'
PROCESSEDPATH='/iris/DATA1/out/dlu/CD/processed'
ARCHIVEPATH='/iris/DATA1/out/dlu/CD/archive/'

TMP1='.tmp1'
TMP2='.tmp2'
TMP3='.tmp3'

#############################
# Main
#############################
cd $INPATH
INFILES=`ls tblGESTORES_*.zip`
NFILES=`find $INPATH -maxdepth 1 -name "tblGESTORES_*.zip" | wc -l`
DONE='.dat'

for FILENAME in $INFILES
do
	echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Uncompressing file ${FILENAME}" >> ${LOG}
    unzip $FILENAME
	BASEFILE='tblGESTORES.txt'
	TMP1FILE=$INPATH$BASEFILE$TMP1
	TMP2FILE=$INPATH$BASEFILE$TMP2
	TMP3FILE=$INPATH$BASEFILE$TMP3

	cat $BASEFILE > $TMP1FILE
	   
	awk 'BEGIN {FS=";"; OFS=";"} {
		aux2=";;"
		aux6=";;;;;;"
		$5=$5aux2
		$6=$6aux6
		$7=$7aux6
		$8=$8aux6
		$9=$9aux6
		$10=substr($10,1,length($10)-1)";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;"
		print $0}' $TMP1FILE > $TMP2FILE	
		
	awk 'BEGIN {FS=";"; OFS=";"} { printf "%s\r\n", $0}' $TMP2FILE > $TMP3FILE	
		     
    tail -n +2 $TMP3FILE | tr -d '\"' > DUNBGEST_F_`date +%Y%m%d%H%M%S`$DONE

    for FILE in DUNBGEST_F_*.dat
    do
	cp $FILE $PROCESSEDPATH
    mv $FILE $OUTPATH
	echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Generated file ${FILENAME}: ${FILE} and saved in ${PROCESSEDPATH}" >> ${LOG}
    done

	rm -rf tblGESTORES.txt
	rm $TMP1FILE
	rm $TMP2FILE
	rm $TMP3FILE
		
	mv $FILENAME $ARCHIVEPATH
	echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Archived file ${FILENAME} to ${ARCHIVEPATH} " >> ${LOG}
		
done

if [ $NFILES -eq 0 ]; then
	echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - No files found to process" >> ${LOG}
fi

###END#############################################################################################






