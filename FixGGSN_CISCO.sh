#!/bin/bash
#
##################################################################################################
#
# NAME
#   FixGGSN_CISCO.sh
#
# DESCRIPTION
#       - Fix GGSN usage files
#
# INPUTS
#   (none)
#
# OUTPUTS
#   (none)
#
# REMARKS
#   (none)
#
# ------------------------------------------------------------------------------------------
# DATE:        26-11-2014
# AUTHOR:      PT DBSS - Daniel Duro (daniel-r-duro@telecom.pt)
# DESCRIPTION: version 1.00
#
# DATE:        25-02-2022
# AUTHOR:      DIT - Diamantino Carreira (diamantino-p-carreira@telecom.pt)
# DESCRIPTION: Layout the input file change
###################################################################################################
#set -n   # Uncomment to check sintax, without execution.
#set -x   # Uncomment to debug this shell script

#############################
# Global interface
#############################

. /iris/scripts/utils.sh

#############################
# Variaveis Locais
#############################

SCRIPTNAME=$(basename $0)


PATHLOG=${BASELOG}/preprocess_scripts
LOG=${PATHLOG}/log.txt.${year}-${month}-${day}

INPATH='/iris/DATA1/in/I-GGSN_CISCO/'
OUTPATH='/iris/DATA1/in/I-GGSN/'
ARCHIVEPATH='/iris/DATA1/in/I-GGSN_CISCO/archive'

#############################
# Main
#############################
cd $INPATH
INFILES=`ls EPC*.gz`
READY='.dat'
TMP1='.tmp1'
TMP2='.tmp2'

for FILENAME in $INFILES
do
		cp $FILENAME $ARCHIVEPATH
		echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Archived file ${FILENAME} to ${ARCHIVEPATH} " >> ${LOG}
		gzip -d  $FILENAME
		echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Uncompressing file ${FILENAME}" >> ${LOG}
		
		TXTFILES=`ls EPC*.cdr`
        for TXTFN in $TXTFILES
        do
		
			fileNameLen=`echo $TXTFN | wc -c`
			fileNameLen=`expr $fileNameLen - 5`
			BASEFILE=`echo $TXTFN | cut -c1-$fileNameLen`
		
			TMP1FILE=$INPATH$BASEFILE$TMP1
			TMP2FILE=$INPATH$BASEFILE$TMP2
			
			cat $TXTFN > $TMP1FILE
			
                sed -i '/EPC*/d' $TMP1FILE

				#Remove last two columns
				awk 'NF{NF-=2}1' FS='|' OFS='|' $TMP1FILE > $TMP2FILE
				
				#Remove tmp files
                mv $TMP2FILE $OUTPATH$BASEFILE$READY			
				x=`ls *.cdr`
                rm $x
				rm $TMP1FILE
				#mv $FILENAME $ARCHIVEPATH
                echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Generated file ${FILENAME} - $BASEFILE$READY" >> ${LOG}
                
        done

done
###END#############################################################################################

