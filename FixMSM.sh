#!/bin/bash
#
##################################################################################################
#
# NAME
#   FixMSM.sh
#
# DESCRIPTION
#       - Fix M2M Msisdn files
#
# INPUTS
#   (none)
#
# OUTPUTS
#   (none)
#
# REMARKS
#   (none)
#
# ------------------------------------------------------------------------------------------
# DATE:        03-06-2014
# AUTHOR:      Cvidya - Ilyia Stanev (Ilyia.Stanev@cvidya.com)
# DESCRIPTION: version 1.00
#
# DATE:        06-08-2014
# AUTHOR:      PT DOS - Joao Pimentel (joao-a-pimentel@telecom.pt)
# DESCRIPTION: Added output to log file
#              Copy the output file to processed folder
#              Archive original file in archive folder
#
# DATE:        14-04-2015
# AUTHOR:      DSI - Diamantino Carreira (diamantino-p-carreira@telecom.pt)
# DESCRIPTION: The input layout changed. 
#              The process doesn't write in output the last two columns
#
# DATE:        24-08-2016
# AUTHOR:      DSI - Diogo Santos (diogo-a-santos@telecom.pt)
# DESCRIPTION: 110029294: Layout change on output file MSM_F. (Removal of imsi2 column) 
#			   New output file created (M2MMSM_MOBILIX*.dsv) with all M2M records each imsi/imsi2
#
# DATE:        21-02-2017
# AUTHOR:      MEO - Fernando Paz (fernando-paz@telecom.pt)
# DESCRIPTION: Script redone so it merges the M2M_MSISDN_0 and M2M_MSISDN_1 files.
#			   Was also created a file history mechanism as well as some file validation logic
#
# DATE:        16-11-2017
# AUTHOR:      MEO - Fernando Paz (fernando-paz@telecom.pt)
# DESCRIPTION: Since M2M_MSISDN_1 file is not received every day the merge process had to be
#              changed so it uses the most recent file from the archive folder
#              
# DATE:	       06-08-2021
# AUTHOR:      MEO - Diogo Santos (diogo-a-santos@telecom.pt)
# DESCRIPTION: Input layout of M2M_MSISDN_1 changed.
#	       Update of merge process to account for layout changes
#              
# DATE:	       16-09-2022
# AUTHOR:      MEO - Fernando Paz (fernando-paz@telecom.pt)
# DESCRIPTION: APN column is now truncated to 50 chars in the MSM_F_0_* output file
#
###################################################################################################
#set -n   # Uncomment to check sintax, without execution.
#set -x   # Uncomment to debug this shell script

#############################
# Global interface
#############################

. /iris/scripts/utils.sh


#############################
# Variaveis Locais
#############################

#SCRIPTNAME=$(basename $0)
SCRIPTNAME="FixMSM"

PATHLOG=${BASELOG}/preprocess_scripts
LOG=${PATHLOG}/log.txt.${year}-${month}-${day}
PATHSPOOL=${BASESPOOL}/${SCRIPTNAME}

INPATH='/iris/DATA1/out/dlu/CD/'
OUTPATH='/iris/DATA1/out/dlu/CD/data/'

PROCESSEDPATH='/iris/DATA1/out/dlu/CD/processed/'
ARCHIVEPATH='/iris/DATA1/out/dlu/CD/archive/'
QUARANTINE_FOLDER="/iris/DATA1/out/dlu/CD/quarentena/"


#Aux Files
FILE_HISTORY="${PATHSPOOL}/file_history.txt"                        # Processed files history
M2M_MSISDN_0_TMPFILE="${PATHSPOOL}/m2m_msisdn_0_files.dat"     		# Tmp file
M2M_MSISDN_1_TMPFILE="${PATHSPOOL}/m2m_msisdn_1_files.dat"     		# Tmp file
SPOOL="${PATHSPOOL}/fixmsm_files_email.dat"							# Tmp file


if [ -e $SPOOL ]; then
	cat /dev/null > $SPOOL
fi


#############################
# Functions
#############################

move_to_quarentena(){
    #args $1 = file name
    
    if [ ! -z $1 ]
    then
        if [ -e $INPATH$1 ]
    	then
            echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Moving file ${1} to quarantene folder." >> ${LOG}
    		mv $INPATH$1 $QUARANTINE_FOLDER
    	else
    		echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Unable to move ${1}. File not found." >> ${LOG}
    	fi
    else
        echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Unable to move the file. Its name was not expecified."
    fi
}


envia_email(){
	echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Alert email sent to the support team." >> ${LOG}
	
	actual_time=`date +"%Y-%m-%d"`
	body1="Apos o merge o ficheiro ${M2M_MSISDN_0} tinha menos de 500000 linhas pelo que foi movido para a pasta de quarentena."
	body2="Validar ficheiros fonte. Em caso de erros nos M2M_MSISDN contatar equipa das INs."

/usr/sbin/sendmail $SUPPORTTEAM << END
Subject: FraudView - Alarme - $SCRIPTNAME - $actual_time
from:$SUPPORTTEAM
to:$SUPPORTTEAM

$message

$body1

$body2



Obrigado,

$SUPPORTTEAMNAME
$SUPPORTTEAM
END
}


#############################
# Main
#############################

mkdir -p ${PATHLOG} ${PATHSPOOL} ${QUARANTINE_FOLDER}

if [ ! -e $FILE_HISTORY ]; then
	echo "20170101" > $FILE_HISTORY
	echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - New history file created at ${PATHSPOOL}" >> ${LOG}
fi

LAST_FILE_DATE=`tail -1 ${FILE_HISTORY}`

if [ `ls ${INPATH}M2M_MSISDN_0_* 2> /dev/null | wc -l` -eq 0 ]; then
    echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - No M2M_MSISDN_0 files were found. Process terminated." >> ${LOG}
    exit

else
	NEWEST_0_FILE_NAME=`basename $(ls ${INPATH}M2M_MSISDN_0_* | sort | tail -1)`
	NEWEST_0_FILE_DATE=$(echo ${NEWEST_0_FILE_NAME} | cut -c14-21)
	
	
	# MOVE OLDER M2M_MSISDN_0 FILES TO QUARANTINE FOLDER
	for c in `ls ${INPATH}M2M_MSISDN_0_*`; do
		AUX_FILE_NAME=`basename $c`
		AUX_FILE_DATE=$(echo ${AUX_FILE_NAME} | cut -c14-21)
		
		if [ $AUX_FILE_DATE -le $LAST_FILE_DATE ]; then
			echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Newer files than ${AUX_FILE_NAME} were already processed." >> ${LOG}
			move_to_quarentena $AUX_FILE_NAME
		else
			if [ $AUX_FILE_DATE -lt $NEWEST_0_FILE_DATE ]; then
				echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - The file ${AUX_FILE_NAME} is older than the ${NEWEST_0_FILE_NAME} already available." >> ${LOG}
				move_to_quarentena $AUX_FILE_NAME
			fi
		fi
	done
	
	
	# EXIT IF THERE ISNT A VALID M2M_MSISDN_0 FILE LEFT IN THE INPUT
	if [ `ls ${INPATH}M2M_MSISDN_0_* 2> /dev/null | wc -l` -eq 0 ]; then
		echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - No valid M2M_MSISDN_0 files were found. Process terminated." >> ${LOG}
		exit
	fi
	
	
	# IF THERE ARE M2M_MSISDN_1 FILES IN THE INPUT MAKE SURE WE ONLY KEEP THE MOST RECENT ONE THERE
	if [ `ls ${INPATH}M2M_MSISDN_1_* 2> /dev/null | wc -l` -gt 0 ]; then
		NEWEST_1_FILE_NAME=`basename $(ls ${INPATH}M2M_MSISDN_1_* | sort | tail -1)`
		NEWEST_1_FILE_DATE=$(echo ${NEWEST_1_FILE_NAME} | cut -c14-21)
		
		NEWEST_1_ARCHIVED_FILE_NAME=`basename $(ls ${ARCHIVEPATH}M2M_MSISDN_1_* | sort | tail -1)`
		NEWEST_1_ARCHIVED_FILE_DATE=$(echo ${NEWEST_1_ARCHIVED_FILE_NAME} | cut -c14-21)
		
		
		# IF THERE IS A MORE RECENT M2M_MSISDN_1 FILE IN ARCHIVE THAN THE ONES IN INPUT MOVE IT INTO INPUT
		if [[ `ls ${ARCHIVEPATH}M2M_MSISDN_1_* 2> /dev/null | wc -l` -gt 0 && "$NEWEST_1_FILE_DATE" -lt "$NEWEST_1_ARCHIVED_FILE_DATE" ]]; then
			mv ${ARCHIVEPATH}${NEWEST_1_ARCHIVED_FILE_NAME} ${INPATH}
			echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - The file ${NEWEST_1_ARCHIVED_FILE_NAME} was moved from archive to input." >> ${LOG}
		fi
		
		NEWEST_1_FILE_NAME=`basename $(ls ${INPATH}M2M_MSISDN_1_* | sort | tail -1)`
		NEWEST_1_FILE_DATE=$(echo ${NEWEST_1_FILE_NAME} | cut -c14-21)
		
		
		# MOVE OLDER M2M_MSISDN_1 FILES TO QUARANTINE FOLDER
		for c in `ls ${INPATH}M2M_MSISDN_1_*`; do
			AUX_FILE_NAME=`basename $c`
			AUX_FILE_DATE=$(echo ${AUX_FILE_NAME} | cut -c14-21)
			
			#if [ $AUX_FILE_DATE -le $LAST_FILE_DATE ]; then
			#	echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Newer files than ${AUX_FILE_NAME} were already processed." >> ${LOG}
			#	move_to_quarentena $AUX_FILE_NAME
			#else
				if [ $AUX_FILE_DATE -lt $NEWEST_1_FILE_DATE ]; then
					echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - The file ${AUX_FILE_NAME} is older than the ${NEWEST_1_FILE_NAME} already available." >> ${LOG}
					move_to_quarentena $AUX_FILE_NAME
				fi
			#fi
		done
	fi
	
	
	# IF THERE ARE NO M2M_MSISDN_1 FILES IN THE INPUT TRY TO FETCH THE MOST RECENT FROM ARCHIVE
	if [[ `ls ${INPATH}M2M_MSISDN_1_* 2> /dev/null | wc -l` -eq 0 && `ls ${ARCHIVEPATH}M2M_MSISDN_1_* 2> /dev/null | wc -l` -gt 0 ]]; then
		echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - There are no M2M_MSISDN_1 files in input. Fetching most recent from archive." >> ${LOG}
		NEWEST_1_ARCHIVED_FILE_NAME=`basename $(ls ${ARCHIVEPATH}M2M_MSISDN_1_* | sort | tail -1)`
		mv ${ARCHIVEPATH}${NEWEST_1_ARCHIVED_FILE_NAME} ${INPATH}
		echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Moved $NEWEST_1_ARCHIVED_FILE_NAME from archive to input." >> ${LOG}
		NEWEST_1_FILE_NAME=`basename $(ls ${INPATH}M2M_MSISDN_1_* | sort | tail -1)`
		NEWEST_1_FILE_DATE=$(echo ${NEWEST_1_FILE_NAME} | cut -c14-21)
	fi
	
	
	# IF THERE ARE NO M2M_MSISDN_1 FILES IN INPUT EXITS
	if [ `ls ${INPATH}M2M_MSISDN_1_* 2> /dev/null | wc -l` -eq 0 ]; then
		echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - There are no M2M_MSISDN_1 files to process. Process terminated." >> ${LOG}
		exit
	fi
	
	
	# WAIT A MINUTE TO MAKE SURE THE FILES FINISHED TRANSFERING AND ARE COMPLETE
    #sleep 60
	
	
	# ASSIGN THE FINAL PAIR OF FILE NAMES TO NEW VARIABLES
	M2M_MSISDN_0=${NEWEST_0_FILE_NAME}
	M2M_MSISDN_1=${NEWEST_1_FILE_NAME}
	
	FILE_EXT0="${M2M_MSISDN_0##*.}"
	FILE_EXT1="${M2M_MSISDN_1##*.}"
	
	
	# BACKUP THE ORIGINAL FILES. CREATE THE FOLDER IF IT DOESNT EXIST YET
	if [ ! -d "$ARCHIVEPATH" ]; then
		mkdir ${ARCHIVEPATH}
	fi
	
	cp -p ${INPATH}${M2M_MSISDN_0} ${ARCHIVEPATH}
	cp -p ${INPATH}${M2M_MSISDN_1} ${ARCHIVEPATH}
	
	echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Archived ${M2M_MSISDN_0} to ${ARCHIVEPATH}" >> ${LOG}
	echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Archived ${M2M_MSISDN_1} to ${ARCHIVEPATH}" >> ${LOG}
	
	
	# UNCOMPRESSES M2M_MSISDN_0 FILE IF COMPRESSED
    case $FILE_EXT0 in
        "gz"|"GZ")
            gunzip ${INPATH}${M2M_MSISDN_0}
        ;;
        
        "tar"|"TAR")
            tar xvfz ${INPATH}${M2M_MSISDN_0}
        ;;
        
        "zip"|"ZIP")
            7za x -o ${INPATH}${M2M_MSISDN_0}
        ;;
    esac
	
	# RE-ASSIGN VARIABLE TO REFRESH ITS EXTENSION
	M2M_MSISDN_0=`basename $(ls ${INPATH}${M2M_MSISDN_0%.*}* | tail -1)`
	
	
	# UNCOMPRESSES M2M_MSISDN_1 FILE IF COMPRESSED
	case $FILE_EXT1 in
        "gz"|"GZ")
            gunzip ${INPATH}${M2M_MSISDN_1}
        ;;
        
        "tar"|"TAR")
            tar xvfz ${INPATH}${M2M_MSISDN_1}
        ;;
        
        "zip"|"ZIP")
            7za x -o ${INPATH}${M2M_MSISDN_1}
        ;;
    esac
	
	# RE-ASSIGN VARIABLE TO REFRESH ITS EXTENSION
	M2M_MSISDN_1=`basename $(ls ${INPATH}${M2M_MSISDN_1%.*}* | tail -1)`
	
	# RECONFIGURE LAYOUT of M2M_MSISDN_1 to match layout of M2M_MSISDN_0
	awk 'BEGIN {FS = ";" ; OFS=";"} ; 
			FNR==1 { print $1,$2,$4,$5,$6,$7,$8,$9,$10,$11,"apn",$12,$14;} ; 
			FNR>1 { print $1,$2,$4,$5,$6,$7,$8,$9,$10,$11,"",$12,$14;}' ${INPATH}${M2M_MSISDN_1} > ${INPATH}${M2M_MSISDN_1}.tmp
	mv ${INPATH}${M2M_MSISDN_1}.tmp ${INPATH}${M2M_MSISDN_1}	
	
	echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Merging $M2M_MSISDN_0 and $M2M_MSISDN_1 files." >> ${LOG}
	
	
	# REMOVE THE FILES HEADER
	sed -i 1d ${INPATH}${M2M_MSISDN_0}
	sed -i 1d ${INPATH}${M2M_MSISDN_1}
	
	
	# MERGE THE FILES
	cat ${INPATH}${M2M_MSISDN_1} >> ${INPATH}${M2M_MSISDN_0}
	rm ${INPATH}${M2M_MSISDN_1}
	
	
	
	file_length_merged=`cat ${INPATH}${M2M_MSISDN_0} | wc -l`
	
	
	# CHECK THE MERGED FILE SIZE
	#if [ $file_length_merged -gt 500000 ]; then
	if [ $file_length_merged -gt 50000 ]; then	
		READY='dat'
		READY2='dsv'
		TMP1='tmp1'
		TMP2='tmp2'
		DONE='.done'
		
		fileNameLen=`echo ${M2M_MSISDN_0} | wc -c`
		fileNameLen=`expr ${fileNameLen} - 4`
		BASEFILE=`echo ${M2M_MSISDN_0} | cut -c1-${fileNameLen}`
		TMP1FILE=${OUTPATH}${BASEFILE}${TMP1}
		TMP2FILE=${OUTPATH}${BASEFILE}${TMP2}
		READYFILE=${OUTPATH}${BASEFILE}${READY}
		
		echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Processing file ${M2M_MSISDN_0}" >> ${LOG}
		DONEFILENAME=${BASEFILE/M2M_MSISDN/MSM_F}
		
		#RF001
		cat ${INPATH}${M2M_MSISDN_0} > ${TMP1FILE}
		
		awk 'BEGIN {FS=";";OFS=";"} {print($1,$2,$3,$4,$6,$11)}' ${TMP1FILE} > ${TMP1FILE}${TMP2}
		
		#mv ${TMP1FILE}${TMP2} ${OUTPATH}${DONEFILENAME}${READY}
		
		# TRUNCATE THE 6TH COLUMN TO 50 CHARS AND GENERATE THE FINAL FILE : MSM_F_0_*.dat
		awk -F';' 'BEGIN { OFS=";" } { print $1,$2,$3,$4,$5,substr($6,1,50) }' ${TMP1FILE}${TMP2} > ${OUTPATH}${DONEFILENAME}${READY}
		rm ${TMP1FILE}${TMP2}
		cp -p  ${OUTPATH}${DONEFILENAME}${READY} ${ARCHIVEPATH}${DONEFILENAME}${READY}
		echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - File ${M2M_MSISDN_0} truncated and renamed to ${DONEFILENAME}${READY}" >> ${LOG}
		
		DONEFILENAME2=${BASEFILE/M2M_MSISDN/M2MMSM_MOBILIX}
		
		#RF002
		cat ${INPATH}${M2M_MSISDN_0} > ${TMP2FILE}
		
		awk 'BEGIN {FS = ";" ; OFS="|"} ; 
			FNR>1 {
				print $4,$2,"",$12,"","","","","","","","""|"; 
				n=split($5,a,"|"); for (i=1; i<=n; i++) print a[i],$2,"",$12,"","","","","","","","""|"}' ${TMP2FILE} > ${TMP2FILE}${TMP1}
		
		
		mv ${TMP2FILE}${TMP1} ${INPATH}${DONEFILENAME2}${READY2}
		echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Generated File ${INPATH}${DONEFILENAME2}${READY2}" >> ${LOG}
		cp -p  ${INPATH}${DONEFILENAME2}${READY2} ${ARCHIVEPATH}${DONEFILENAME2}${READY2}
		
		mv ${INPATH}${M2M_MSISDN_0} ${ARCHIVEPATH}${M2M_MSISDN_0}.MERGED
		echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Archived file ${M2M_MSISDN_0} to ${ARCHIVEPATH} " >> ${LOG}
		
		rm ${TMP1FILE}
		rm ${TMP2FILE}
		
		
		# UPDATE THE CONTROL FILE
		echo ${NEWEST_0_FILE_DATE} >> ${FILE_HISTORY}
		
		echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Process finished." >> ${LOG}
		
	else
		# MOVE THE MERGE FILE TO QUARENTINE
		mv ${INPATH}${M2M_MSISDN_0} ${QUARANTINE_FOLDER}${M2M_MSISDN_0}.MERGED
		
		# MOVE THE MERGED FILES FROM ARCHIVE TO QUARENTINE
		mv ${ARCHIVEPATH}${M2M_MSISDN_0}* ${QUARANTINE_FOLDER}
		mv ${ARCHIVEPATH}${M2M_MSISDN_1}* ${QUARANTINE_FOLDER}
		
		echo "$(date +"%Y%m%d%H%M%S")  ERROR - ${SCRIPTNAME} - The merged file ${M2M_MSISDN_0} has less than 500000 records. Process failed." >> ${LOG}
		echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - File was moved to ${QUARANTINE_FOLDER}" >> ${LOG}
		
		# SEND ALERT EMAIL
		envia_email
	fi
	
fi


###END#############################################################################################


