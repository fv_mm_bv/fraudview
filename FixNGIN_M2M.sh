#!/bin/bash
#
##################################################################################################
#
# NAME
#   FixNGIN_M2M.sh
#
# DESCRIPTION
#       - Fix NGIN files
#
# INPUTS
#   (none)
#
# OUTPUTS
#   (none)
#
# REMARKS
#   (none)
#
# ------------------------------------------------------------------------------------------
# DATE:        13-11-2016
# AUTHOR:      PT DBSS - Diamantino Carreira (diamantino-p-carreira@telecom.pt)
# BASE :       FixNGIN_M2M.sh
# DESCRIPTION: Fix Script for NGIN_M2M Files
#
# DATE:        30-11-2016
# AUTHOR:      PT MEO - Fernando Paz (fernando-paz@telecom.pt)
# DESCRIPTION: Added a copy of the original files to a MoneyMap folder
#
# DATE:        02-03-2017
# AUTHOR:      MEO - Diamantino Carreira (diamantino-p-carreira@telecom.pt)
# DESCRIPTION: Change Filter rules
#
# DATE:        27-10-2017
# AUTHOR:      MEO - Diamantino Carreira (diamantino-p-carreira@telecom.pt)
# DESCRIPTION: Change Filter rules
#
# DATE:        30-03-2022
# AUTHOR:      MEO - Diamantino Carreira (diamantino-p-carreira@telecom.pt)
# DESCRIPTION: Change field Campovariavle37
#
###################################################################################################
#set -n   # Uncomment to check sintax, without execution.
#set -x   # Uncomment to debug this shell script

#############################
# Global interface
#############################
. /iris/scripts/utils.sh
#############################
# Variaveis Locais
#############################

SCRIPTNAME=$(basename $0)


PATHLOG=${BASELOG}/preprocess_scripts
LOG=${PATHLOG}/log.txt.${year}-${month}-${day}

INPATH='/iris/DATA1/in/I-NGIN_FUT/'
OUTPATH='/iris/DATA1/in/I-NGIN_FUT/'
ARCHIVEPATH='/iris/DATA1/in/I-NGIN_FUT/archive'
MONEYMAPPATH='/iris/DATA1/in/I-NGIN_FUT/moneymap/'

#############################
# Main
#############################
cd $INPATH
INFILES=`ls NGIN_M2M*.cdr`
NFILES=`find $INPATH -maxdepth 1 -name "NGIN_M2M*.cdr" | wc -l`
READY='.dat'
TMP1='.tmp1'
TMP2='.tmp2'
TMP3='.tmp3'
TMP4='.tmp4'
TMP5='.tmp5'
for FILENAME in $INFILES
do

    
    fileNameLen=`echo $FILENAME | wc -c`
    fileNameLen=`expr $fileNameLen - 5`
    BASEFILE=`echo $FILENAME | cut -c1-$fileNameLen`
	TMP1FILE=$OUTPATH$BASEFILE$TMP1
	TMP2FILE=$OUTPATH$BASEFILE$TMP2
	TMP3FILE=$OUTPATH$BASEFILE$TMP3
	TMP4FILE=$OUTPATH$BASEFILE$TMP4
	
	cp -p $FILENAME $MONEYMAPPATH
	
	cat $FILENAME > $TMP1FILE
     
	
	sed -i '/NGIN_M2M*/d' $TMP1FILE

	awk 'BEGIN {FS="|"} {aux=substr($108,0,5)
	aux2=substr($108,0,6)
	aux3=substr($104,0,3)
	aux4=substr($7,0,9)
	if (($78 == "R") || ((aux ==":204:" || $108 ~/;:204:/) || (aux2 ==":3550:" || $108 ~/;:3550:/)) || ($4 =="OAPI" && $12 == "200") || 
		($78 == "" && aux3 == "208") || (aux4== "208090025") || (aux4== "268066691")) print $0}' $TMP1FILE > $TMP2FILE
		
	# Change field Operador Virtual
	awk 'BEGIN {FS="|" ; OFS="|"} {if ($116 == null) $116="00"; print $0;}' $TMP2FILE > $TMP3FILE

	# Change field Campovariavle37 (SP_CHAR1 )
	awk 'BEGIN {FS="|" ; OFS="|"} {if ($69 == null) $69=" "; print $0;}' $TMP3FILE > $TMP4FILE
	
	mv $TMP4FILE $OUTPATH$BASEFILE$READY
	
	rm $TMP1FILE
	rm $TMP2FILE
	rm $TMP3FILE
	echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Generated file ${FILENAME} - $BASEFILE$READY" >> ${LOG}
	
	mv $FILENAME $ARCHIVEPATH
	echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Archived file ${FILENAME} to ${ARCHIVEPATH} " >> ${LOG}
	
done

if [ $NFILES -eq 0 ]; then
	echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - No files found to process" >> ${LOG}
fi
###END#############################################################################################
