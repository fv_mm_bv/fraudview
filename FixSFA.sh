#!/bin/bash
#
##################################################################################################
#
# NAME
#   FixSFA.sh
#
# DESCRIPTION
#       - Fix SFA files
#
# INPUTS
#   (none)
#
# OUTPUTS
#   (none)
#
# REMARKS
#   (none)
#
# ------------------------------------------------------------------------------------------
# DATE:        05-08-2014
# AUTHOR:      PT DBSS - Daniel Duro (daniel-r-duro@telecom.pt)
# DESCRIPTION: Added output to log file
#              Copy the output file to processed folder
#              Archive original file in archive folder
# 
# DATE:        16-03-2016
# AUTHOR:      PT DBSS - Diogo Santos (diogo-a-santos@telecom.pt)
# DESCRIPTION: Added logic to exclude records with IV_Modificado_Por = SirelNotifUser/Admin (Pedido Catálogo 220002148) 
#	       	   Number of records excluded saved in log file
#
# DATE:        18-07-2016
# AUTHOR:      PT DBSS - Diogo Santos (diogo-a-santos@telecom.pt)
# DESCRIPTION: Excluir as vendas com data de criação menor do que o dia (n-1), sendo (n) a data do ficheiro (Pedido Catálogo 220004371) 
#
# DATE:        02-01-2017
# AUTHOR:      PT DBS - Diamantino Carreira (diamantino-p-carreira@telecom.pt)
# DESCRIPTION: Eliminar os esapços no fim dos campos da morada de faturação 	       	   
#
# DATE:        02-01-2017
# AUTHOR:      PT DBS - Diogo Santos (diogo-a-santos@telecom.pt)
# DESCRIPTION: Mudar virgula(,) da coluna IV_PRECO_UNITARIO(#72) para ponto "."
#
# DATE:        26-02-2018
# AUTHOR:      MEO - Fernando Paz (fernando-paz@telecom.pt)
# DESCRIPTION: Quick fix added to move duplicate files to quarantine folder
#
# DATE:        10-05-2019
# AUTHOR:      MEO - Diamantino Carreira (diamantino-p-carreira@telecom.pt)
# DESCRIPTION: Pedido 220021482 
#
# DATE:        20-05-2019
# AUTHOR:      MEO - Diamantino Carreira (diamantino-p-carreira@telecom.pt)
# DESCRIPTION: Pedido 220021752 - Alteracao do filtro de exclusao de registos 
#
# DATE:        14-02-2020
# AUTHOR:      MEO - Diamantino Carreira (diamantino-p-carreira@telecom.pt)
# DESCRIPTION: Adicionado envio de email quando o ficheiro ? considerado duplicado
#
# DATE:        18-05-2022
# AUTHOR:      MEO - Diamantino Carreira (diamantino-p-carreira@telecom.pt)
# DESCRIPTION: Layout dos ficheiros de input foi alterado, foram adicionados 5 campos
#              no final do registo (Pedido 220037586)
#
###################################################################################################
#set -n   # Uncomment to check syntax, without execution.
#set -x   # Uncomment to debug this shell script

#############################
# Global interface
#############################

. /iris/scripts/utils.sh

#############################
# Variaveis Locais
#############################

SCRIPTNAME=$(basename $0)


PATHLOG=${BASELOG}/preprocess_scripts
LOG=${PATHLOG}/log.txt.${year}-${month}-${day}

INPATH='/iris/DATA1/out/dlu/SO/'
OUTPATH='/iris/DATA1/out/dlu/SO/data/'
ARCHIVEPATH='/iris/DATA1/out/dlu/SO/archive/'

#############################
# Variaveis Send mail
#############################
actual_day=`date +"%Y-%m-%d"`
email_suporte=$SUPPORTTEAM
email_suporte_2="$SUPPORTTEAM,$FRAUDTEAM"


###############
#Functions
###############

envia_email(){
#args $1 = grupo_dist $2 = comment  

echo "$actual_time - $1 - ERRO - A enviar email... "

/usr/sbin/sendmail -f $SUPPORTTEAM $email_suporte_2 << END
Subject: FraudView - Alarme - Portal SFA: ficheiro duplicado - $actual_day
from:$SUPPORTTEAM
to:$SUPPORTTEAM;$FRAUDTEAM

$message

$2

Obrigado,

$SUPPORTTEAMNAME
$SUPPORTTEAM

END

}


#############################
# Main
#############################

# Quick fix to move duplicate files to quarantine folder - 20180226
cd ${INPATH}INCOMING

INFILES=`ls Vendas_Portal*.txt`

for FILENAME in $INFILES
do
	FILENAMESHORT=$(echo $FILENAME | cut -f 1 -d '.')
	
	if [ `ls ${INPATH}archive/${FILENAMESHORT}* 2> /dev/null | wc -l` -eq 0 ]; then
		mv ${INPATH}INCOMING/${FILENAME} ${INPATH}
	else
		echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - ${FILENAME} is a duplicate. Moving to ${INPATH}quarentena/." >> ${LOG}
		mv -f ${INPATH}INCOMING/${FILENAME} ${INPATH}quarentena/
		envia_email $SCRIPTNAME "O ficheiro:  ${FILENAME}  foi considerado duplicado"
	fi
done
# End of Quick fix - 20180226


cd ${INPATH}

INFILES=`ls Vendas_Portal*.txt`
NFILES=`find $INPATH -maxdepth 1 -name "Vendas_Portal*.txt" | wc -l`
READY='.txt'
TMP1='.tmp1'
TMP2='.tmp2'
TMP3='.tmp3'
TMP4='.tmp4'
TMP5='.tmp5'
TMP6='.tmp6'
TMP7='.tmp7'
TMP8='.tmp8'
TMP9='.tmp9'
TMP10='.tmp10'
TMP11='.tmp11'
TMP12='.tmp12'
TMP13='.tmp13'

for FILENAME in $INFILES
do

# FIXO
if [ "${FILENAME:0:22}" == "Vendas_Portal_SFA_Fixo" ];
then

	TEMP1="SO_SFA_"${FILENAME:23:8}"`date +'%H%M%S'`_0001"
	TMPFILE1=$OUTPATH$TEMP1$TMP1
	cat $FILENAME > $TMPFILE1
		 
	iconv -c -f utf-16 -t utf-8 $TMPFILE1 > $TEMP1$TMP2

##Contador IV_Modificado_Por = SirelNotifUser/Admin -> LOG
        
	COUNT=$(awk -v OFS=§ -F§ '{
		aux1 = substr($78,1,10)
		aux3 = substr($80,1,10)
        if ((($81 == "Admin") && (aux1 != aux3)) || (($81 == "SirelNotifUser") && (aux1 != aux3)))
        {COUNT++;}} END {print COUNT}' < $TEMP1$TMP2) 

## Inicio 
	#Pedido Catalogo 220004371
	Fdate="${FILENAME:23:4}"-"${FILENAME:27:2}"-"${FILENAME:29:2}"

	awk -v fdate="$Fdate" -v OFS=§ -F§ '{
	if (fdate==substr($27,1,10)) print }' $TEMP1$TMP2 > $TEMP1$TMP3

	awk -v OFS=§ -F§ '{
		aux1 = substr($78,1,10)
		aux3 = substr($80,1,10)
		if ((($81 == "Admin") && (aux1 != aux3)) || (($81 == "SirelNotifUser") && (aux1 != aux3)))
		next
		else
		print }' $TEMP1$TMP3 > $TEMP1$TMP4

	## Change comma to dot na coluna IV_PRECO_UNITARIO(#72)
 	awk -v OFS=§ -F§ '{
		aux1 = substr($11,1,5)
		aux3 = (gsub(" ","",aux1)" ")
		$11 = aux1" "substr($11,6,105)"§" substr($11,111,12)"§" substr($11,123,8)"§" substr($11,131,8)
		sub(",",".",$72)
		print }' $TEMP1$TMP4 > $TEMP1$TMP5

	awk -v OFS=§ -F§ '{
		aux1 = substr($25,1,5)
		aux3 = (gsub(" ","",aux1)" ")
		$25 = aux1" "substr($25,6,105)"§" substr($25,111,12)"§" substr($25,123,8)"§" substr($25,131,8)
		print }' $TEMP1$TMP5 > $TEMP1$TMP6

	awk -v OFS=§ -F§ '{
		aux1 = substr($40,1,5)
		aux3 = (gsub(" ","",aux1)" ")
		aux4 = substr($40,6,50)
		aux5 = (gsub(/[ \t]+$/,"",aux4)" ")
		aux6 = substr($40,111,12)
		aux7 = (gsub(/[ \t]+$/,"",aux6)" ")
		aux8 = substr($40,123,8)
		aux9 = (gsub(/[ \t]+$/,"",aux8)" ")
		aux10 = substr($40,131,8)
		aux11 = (gsub(/[ \t]+$/,"",aux10)" ")
		$40 = aux1" "aux4"§" aux6"§" aux8"§" aux10
		print }' $TEMP1$TMP6 > $TEMP1$TMP7	
		
	awk -v OFS=§ -F§ '{
		aux1 = substr($95,1,4)
        aux3 = (gsub(" ","",aux1)" ")
		$95 = aux1
        print }' $TEMP1$TMP7 > $TEMP1$TMP8
	
	##Pedido Catalogo 220003498
	awk -v OFS=§ -F§ '{
		aux1 = substr($37,1,3) "" substr($37,6,37) "" substr($37,111,11) "" substr($37,123,5) "" substr($37,131,6)
		aux2 = (gsub(/[ \t]+$/,"",aux1)" ")
		$37 = aux1
		sub("-","",$38)
		sub("-","",$44)
        print }' $TEMP1$TMP8 > $TEMP1$TMP9
		
	##Pedido 220021482 
	awk -v OFS=§ -F§ '{
		aux1 = "§""§""§""§""§""§"substr($96,1,length($96)-1)
		$96 = aux1
        print }' $TEMP1$TMP9 > $TEMP1$TMP10
		
	##
	awk -v OFS=§ -F§ '{
		aux1 = substr($73,1,80)
		$73 = aux1
        print }' $TEMP1$TMP10 > $TEMP1$TMP11
		
	##Pedido 220037586
	awk -v OFS=§ -F§ '{
		aux1 = $106
		$105 = aux1
		$106=$107=""
		print }' $TEMP1$TMP11 > $TEMP1$TMP12
		
	iconv -c -f utf-8 -t cp1252 $TEMP1$TMP12 > $TEMP1$TMP13
	
	 mv $TEMP1$TMP13 $OUTPATH$TEMP1$READY
	 
	 rm $TMPFILE1
	 rm $TEMP1$TMP2
	 rm $TEMP1$TMP3
	 rm $TEMP1$TMP4
	 rm $TEMP1$TMP5
	 rm $TEMP1$TMP6
	 rm $TEMP1$TMP7
	 rm $TEMP1$TMP8
	 rm $TEMP1$TMP9
	 rm $TEMP1$TMP10
	 rm $TEMP1$TMP11
	 rm $TEMP1$TMP12

if [[ -z "${COUNT// }" ]]; then
COUNT=0
fi
	
        echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Input file ${FILENAME} has ${COUNT} lines to be excluded" >> ${LOG}
	echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Generated file ${FILENAME} - $TEMP1$READY" >> ${LOG}
	
fi

## MOVEL
if [  "${FILENAME:0:23}" == "Vendas_Portal_SFA_Movel"  ];
then 
	
	TEMP2="SO_SFA_"${FILENAME:24:8}"`date +'%H%M%S'`_0002"
	TMPFILE2=$OUTPATH$TEMP2$TMP1

        cat $FILENAME > $TMPFILE2 
	
	iconv -c -f utf-16 -t utf-8 $TMPFILE2 > $TEMP2$TMP2

##Contador IV_Modificado_Por = SirelNotifUser/Admin -> LOG
	COUNT=$(awk -v OFS=§ -F§ '{
		aux1 = substr($78,1,10)
		aux3 = substr($80,1,10)
		if ((($81 == "Admin") && (aux1 != aux3)) || (($81 == "SirelNotifUser") && (aux1 != aux3)))
		{COUNT++;}} END {print COUNT}' < $TEMP2$TMP2)

#Inicio
	#Pedido Catalogo 220004371	
	Fdate="${FILENAME:24:4}"-"${FILENAME:28:2}"-"${FILENAME:30:2}"

	awk -v fdate="$Fdate" -v OFS=§ -F§ '{
		if (fdate==substr($27,1,10)) print }' $TEMP2$TMP2 > $TEMP2$TMP3

        #
	awk -v OFS=§ -F§ '{	
		aux1 = substr($78,1,10)
		aux3 = substr($80,1,10)
		if ((($81 == "Admin") && (aux1 != aux3)) || (($81 == "SirelNotifUser") && (aux1 != aux3)))
		next
		else
		print }' $TEMP2$TMP3 > $TEMP2$TMP4
	
	awk -v OFS=§ -F§ '{
		aux1 = substr($11,1,5)
		aux3 = (gsub(" ","",aux1)" ")
		$11 = aux1" "substr($11,6,105)"§" substr($11,111,12)"§" substr($11,123,8)"§" substr($11,131,8)
		print }' $TEMP2$TMP4 > $TEMP2$TMP5

	awk -v OFS=§ -F§ '{
        aux1 = substr($25,1,5)
        aux3 = (gsub(" ","",aux1)" ")
        $25 = aux1" "substr($25,6,105)"§" substr($25,111,12)"§" substr($25,123,8)"§" substr($25,131,8)
        print }' $TEMP2$TMP5 > $TEMP2$TMP6

	awk -v OFS=§ -F§ '{
		aux1 = substr($40,1,5)
		aux3 = (gsub(" ","",aux1)" ")
		aux4 = substr($40,6,50)
		aux5 = (gsub(/[ \t]+$/,"",aux4)" ")
		aux6 = substr($40,111,12)
		aux7 = (gsub(/[ \t]+$/,"",aux6)" ")
		aux8 = substr($40,123,8)
		aux9 = (gsub(/[ \t]+$/,"",aux8)" ")
		aux10 = substr($40,131,8)
		aux11 = (gsub(/[ \t]+$/,"",aux10)" ")
		$40 = aux1" "aux4"§" aux6"§" aux8"§" aux10
		print }' $TEMP2$TMP6 > $TEMP2$TMP7
		
		
	awk -v OFS=§ -F§ '{
		aux1 = substr($95,1,5)
		aux3 = (gsub(" ","",aux1)" ")
		$95 = aux1
		print }' $TEMP2$TMP7 > $TEMP2$TMP8
	
  ##Pedido Catalogo 220003498
        awk -v OFS=§ -F§ '{
			aux1 = substr($37,1,3) "" substr($37,6,37) "" substr($37,111,11) "" substr($37,123,5) "" substr($37,131,6)
			aux2 = (gsub(/[ \t]+$/,"",aux1)" ")
			$37 = aux1
            sub("-","",$38)
            sub("-","",$44)
			print }' $TEMP2$TMP8 > $TEMP2$TMP9
			
	##Pedido 220021482 
	awk -v OFS=§ -F§ '{
		aux1 = "§""§""§""§""§""§"substr($96,1,length($96)-1)
		$96 = aux1
        print }' $TEMP2$TMP9 > $TEMP2$TMP10
		
	
	##
	awk -v OFS=§ -F§ '{
		aux1 = substr($73,1,80)
		$73 = aux1
        print }' $TEMP2$TMP10 > $TEMP2$TMP11
		
	##Pedido 220037586
	awk -v OFS=§ -F§ '{
		aux1 = $106
		$105 = aux1
		$106=$107=""
        print }' $TEMP2$TMP11 > $TEMP2$TMP12

	 iconv -c -f utf-8 -t cp1252 $TEMP2$TMP12 > $TEMP2$TMP13

	 
	mv $TEMP2$TMP13 $OUTPATH$TEMP2$READY

	
 	rm $TMPFILE2
    rm $TEMP2$TMP2
    rm $TEMP2$TMP3
	rm $TEMP2$TMP4
	rm $TEMP2$TMP5
	rm $TEMP2$TMP6
	rm $TEMP2$TMP7
	rm $TEMP2$TMP8
	rm $TEMP2$TMP9
	rm $TEMP2$TMP10
	rm $TEMP2$TMP11
	rm $TEMP2$TMP12

if [[ -z "${COUNT// }" ]]; then 
COUNT=0
fi

echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Input file ${FILENAME} has ${COUNT} lines to be excluded" >> ${LOG}
echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Generated file ${FILENAME} - $TEMP2$READY" >> ${LOG}

fi
mv $FILENAME  $ARCHIVEPATH
	echo -e "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Archived file ${FILENAME} to ${ARCHIVEPATH}\n" >> ${LOG}
done




if [ $NFILES -eq 0 ]; then
echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - No files found to process" >> ${LOG}
fi
###END#############################################################################################

