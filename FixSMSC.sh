#!/bin/bash
#
##################################################################################################
#
# NAME
#   FixSMSC.sh
#
# DESCRIPTION
#       - Fix SMSC files
#
# INPUTS
#   (none)
#
# OUTPUTS
#   (none)
#
# REMARKS
#   (none)
#
# ------------------------------------------------------------------------------------------
# DATE:        05-08-2014
# AUTHOR:      PT DBSS - Daniel Duro (daniel-r-duro@telecom.pt)
# DESCRIPTION: Added output to log file
#              Copy the output file to processed folder
#              Archive original file in archive folder
# DATE:        27-09-2022
# AUTHOR:      PT DIT/RMA - Diamantino Carreir (diamantino-p-carreira@telecom.pt)
# DESCRIPTION: Change filter rules	
#
# DATE:        21-11-2022
# AUTHOR:      PT DIT/RMA - Diamantino Carreir (diamantino-p-carreira@telecom.pt)
# DESCRIPTION: Change filter rules ($a == "E1" && $b == "00")	
#
###################################################################################################
#set -n   # Uncomment to check sintax, without execution.
set +x   # Uncomment to debug this shell script

#############################
# Global interface
#############################

. /iris/scripts/utils.sh

#############################
# Variaveis Locais
#############################

SCRIPTNAME=$(basename $0)


PATHLOG=${BASELOG}/preprocess_scripts
LOG=${PATHLOG}/log.txt.${year}-${month}-${day}

INPATH='/iris/DATA1/in/I-SMSC/'
OUTPATH='/iris/DATA1/in/I-SMSC/'
ARCHIVEPATH='/iris/DATA1/in/I-SMSC/archive/'

#############################
# Main
#############################
cd $INPATH
INFILES=`ls SMSC*.gz`
NFILES=`find $INPATH -maxdepth 1 -name "SMSC*.BILL" | wc -l`
READY='.dat'
TMP1='.tmp1'
TMP2='.tmp2'


for FILENAME in $INFILES
do
	fileNameLen=`echo $FILENAME | wc -c`
    	fileNameLen=`expr $fileNameLen - 9`
    	BASEFILE=`echo $FILENAME | cut -c1-$fileNameLen`
     
        TMP1FILE=$INPATH$BASEFILE$TMP1


        cp $FILENAME $ARCHIVEPATH
        echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Archived file ${FILENAME} to ${ARCHIVEPATH} " >> ${LOG}

        gzip -d  $FILENAME
        echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Uncompressing file ${FILENAME}" >> ${LOG}

        TXTFILES=`ls *.BILL`
        for TXTFN in $TXTFILES
        do
                cat $TXTFN > $TMP1FILE
                x=`ls *.BILL`
                rm $x
		
		IFS=''
		while read line
			do
			a=${line:0:2}
			f=${line:79:5}
		
				if [[ ($a == "E1")  || $a == "F1"  || $f == "21994"  || $f == "21993" ]]
					then
					echo "$line" >> $TMP1FILE$TMP2
				
				fi
			done < $TMP1FILE
		rm $TMP1FILE
		mv $TMP1FILE$TMP2 $OUTPATH$BASEFILE$READY
		echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Generated file ${FILENAME} - $BASEFILE$READY" >> ${LOG}

        done
done


if [ $NFILES -eq 0 ]; then
       echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - No files found to process" >> ${LOG}
fi
###END#############################################################################################
