#!/bin/bash
#
##################################################################################################
#
# NAME
#   FixSWITCH_ERIC.sh
#
# DESCRIPTION
#       - Script preprocess Ericsson files
#
# INPUTS
#   (none)
#
# OUTPUTS
#   (none)
#
# REMARKS
#   (none)
#
# ------------------------------------------------------------------------------------------
# DATE:        28-03-2019
# AUTHOR:      Diamantino Carreira (diamantino-p-carreira@telecom.pt)
# DESCRIPTION: Scrip de pre processamento dos ficheiro da Ericsson 
#
# DATE:        28-11-2019
# AUTHOR:      Diamantino Carreira (diamantino-p-carreira@telecom.pt)
# DESCRIPTION: Filter change 
#  
# DATE:        28-11-2022
# AUTHOR:      Diamantino Carreira (diamantino-p-carreira@telecom.pt)
# DESCRIPTION: Filter SMSMT(Call Type = 30) delivered by MEO's SMS Center (LIKE '351962%')   
#
# DATE:        21-12-2022
# AUTHOR:      Diamantino Carreira (diamantino-p-carreira@telecom.pt)
# DESCRIPTION: Filter SMSMO (call type = 31) AND SMSC MEO (nt_service_centre_address LIKE '35196210%') 
#              with nt_s_m_transmission_result IS NULL 
#  
###########################################################################################################################
#set -n   # Uncomment to check sintax, without execution.
#set -x   # Uncomment to debug this shell script

#############################
# Global interface
#############################

. /iris/scripts/utils.sh

#############################
# Variaveis Locais
#############################

SCRIPTNAME=$(basename $0)


PATHLOG=${BASELOG}/preprocess_scripts
LOG=${PATHLOG}/log.txt.${year}-${month}-${day}

INPATH='/iris/DATA1/in/I-SWITCH_R4/'
OUTPATH='/iris/DATA1/in/I-SWITCH_R4/'
ARCHIVEPATH='/iris/DATA1/in/I-SWITCH_R4/archive/'

#############################
# Main
#############################
cd $INPATH
INFILES=`ls MM*.txt`
NFILES=`find $INPATH -maxdepth 1 -name "MM*.txt" | wc -l`
READY='.dat'
TMP1='.tmp1'
TMP2='.tmp2'
TMP3='.tmp3'
TMP4='.tmp4'
TMP5='.tmp5'
TMP6='.tmp6'
TMP7='.tmp7'
TMP8='.tmp8'
TMP9='.tmp9'
TMP10='.tmp10'

for FILENAME in $INFILES
do
	fileNameLen=`echo $FILENAME | wc -c`
	fileNameLen=`expr $fileNameLen - 5`
	BASEFILE=`echo $FILENAME | cut -c1-$fileNameLen`
    
    
	TMP1FILE=$OUTPATH$BASEFILE$TMP1
	TMP2FILE=$OUTPATH$BASEFILE$TMP2
	TMP3FILE=$OUTPATH$BASEFILE$TMP3
	TMP4FILE=$OUTPATH$BASEFILE$TMP4
	TMP5FILE=$OUTPATH$BASEFILE$TMP5
	TMP6FILE=$OUTPATH$BASEFILE$TMP6
	TMP7FILE=$OUTPATH$BASEFILE$TMP7
	TMP8FILE=$OUTPATH$BASEFILE$TMP8
	TMP9FILE=$OUTPATH$BASEFILE$TMP9
	TMP10FILE=$OUTPATH$BASEFILE$TMP10
	
	cat $FILENAME > $TMP1FILE
	
	awk 'BEGIN {FS="|"} {if (($1 !="P")) print $0}'  $TMP1FILE > $TMP2FILE 
	

	awk 'BEGIN {FS="|"} { if (($58 =="80" || $58 =="81" || $58 =="84"))
				next
				else
				print $0}' $TMP2FILE > $TMP3FILE

	awk 'BEGIN {FS="|"} { if (($2 =="30" && ($3 ~ /26806/) && (($39 ~ /35196/ && length($15)>11) || ($39 ~ /96/ && length($15)>8) || (length($15)==0) || ($39 ~ /35196/ && $15==6027))))
                                next
                                else
                                print $0}' $TMP3FILE > $TMP4FILE
	
	awk 'BEGIN {FS="|"} { if ($2 =="30" && (($15 ~ /^[a-zA-Z]/)  || (substr($15,4,(length($15)-3)) ~ /^[a-zA-Z]/)))
                                next
                                else
                                print $0}' $TMP4FILE > $TMP5FILE	
	
	awk 'BEGIN {FS="|"} { if (($2 =="2" && ($3 ~ /26806/) && (substr($32,1,1)=="#" ||substr($32,1,1)=="B" || substr($32,1,1)=="M" || substr($32,1,1)=="R")))
                                next
                                else
                                print $0}' $TMP5FILE > $TMP6FILE

	awk 'BEGIN {FS="|"} { if (($2 =="26" && ($3 ~ /26806/) && ((substr($32,1,1)=="#" || substr($32,1,1)=="B" || substr($32,1,1)=="M" || substr($32,1,1)=="R") && (substr($18,1,1)=="#" || substr($18,1,1)=="B" || substr($18,1,1)=="M" || substr($18,1,1)=="R"))))
                                next
                                else
                                print $0}' $TMP6FILE > $TMP7FILE
	awk 'BEGIN {FS="|"} { if (($2 =="2" && length($32)==0))
                                next
                                else
                                print $0}' $TMP7FILE > $TMP8FILE
	awk 'BEGIN {FS="|"} { if (($2 =="30" && substr($39,1,8)=="35196210"))
                                next
                                else
                                print $0}' $TMP8FILE > $TMP9FILE
	awk 'BEGIN {FS="|"} { if (($2 =="31" && substr($39,1,8)=="35196210" && $40 ==""))
                                next
                                else
                                print $0}' $TMP9FILE > $TMP10FILE
								
	mv $TMP10FILE $OUTPATH$BASEFILE$READY

	rm $TMP1FILE
	rm $TMP2FILE
	rm $TMP3FILE
	rm $TMP4FILE
	rm $TMP5FILE
	rm $TMP6FILE
	rm $TMP7FILE
	rm $TMP8FILE
	rm $TMP9FILE
	
	echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Generated file ${FILENAME} - $BASEFILE$READY" >> ${LOG}
	
	mv $FILENAME $ARCHIVEPATH
	echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Archived file ${FILENAME} to ${ARCHIVEPATH} " >> ${LOG}
	
done

if [ $NFILES -eq 0 ]; then
	echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - No files found to process" >> ${LOG}
fi
###END#############################################################################################
