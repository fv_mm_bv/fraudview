#!/bin/bash
#
##################################################################################################
#
# NAME
#   Load_Fraudsters_List.sh
#
# DESCRIPTION
#       - Carregamento da lista de moradas problemáticas na tabela F_CD_CUSTOMER_DETAILS.
#
# INPUTS
#   	- FraudsterList_input_AAAAMMDD.csv
#
# OUTPUTS
#   Tabela DBWR.TAB_MORADAS_PROBLEMATICAS
#
# REMARKS
#   (none)
#
# ------------------------------------------------------------------------------------------
# DATE:        03-06-2014
# AUTHOR:      BSS/FRC- Diogo Santos(diogo-a-santos@telecom.pt)
# DESCRIPTION:	Load file Cartoes_Bloqueados_J_AAAAMMDD.csv into table TAB_MSISDN_BLOCKED.
#			   	Copy the input file to processed folder
#				Archive input file in archive folder
#
# DATE:        18-02-2022
# AUTHOR:      RMA/MMR- Diamantino Carreira(diamantino-p-carreira@telecom.pt)
# DESCRIPTION:	Changing the input file layout
#
###################################################################################################
#set -n   # Uncomment to check sintax, without execution.
#set -x   # Uncomment to debug this shell script

#############################
# Global interface
#############################

. $HOME/scripts/utils.sh

#############################
# Variaveis Locais
#############################

SCRIPTNAME=$(basename $0)

PATHLOG=${BASELOG}/preprocess_scripts
LOG=${PATHLOG}/log.txt.${year}-${month}-${day}

INPATH='/iris/DATA1/out/REF'
SQLLDR_PATH='/iris/DATA1/out/REF/data/sqlldr'
ARCHIVEPATH='/iris/DATA1/out/REF/data/archive'

load_Moradas_Prob() {

#Create sqlldr directory if it doesn't exist 
mkdir -p $SQLLDR_PATH

CTL_FILE=$SQLLDR_PATH/"FraudsterList_input.ctl"
#echo $CTL_FILE

echo "load data
CHARACTERSET WE8MSWIN1252
infile '$1' \"str '\n'\"
truncate
into table TAB_MORADAS_PROBLEMATICAS 
fields terminated by ';'
trailing nullcols
	(
	DATA_ATUA DATE \"DD/MM/YYYY\",
	ARRUAMENTO CHAR(100),
	CONTACTO_INSTALACAO CHAR(30),
	CLIENTE CHAR(150),
	CP7 CHAR \"TRIM(:CP7)\",
	EMAIL CHAR(80),
	LOCALIDADE CHAR(100),
	FICHEIRO CONSTANT '$1'
	)" > $CTL_FILE

FILE_NAME=`echo $1 | cut -f 1 -d '.'`
#echo $FILE_NAME
 
		   
if test -s $CTL_FILE; then
	sqlldr $ORA_LOGON CONTROL=$CTL_FILE LOG=$SQLLDR_PATH/sqlldr_$FILE_NAME.log BAD=$SQLLDR_PATH/sqlldr_$FILE_NAME.bad skip=1 > /dev/null 2>&1
	#rm $CTL_FILE
fi

}

insert_customer_details_log () {

nrows=`cat $1 | wc -l`

sqlplus -S $ORA_LOGON << EOF

INSERT INTO F_CD_CUSTOMER_DETAILS_LOG_T VALUES('MORADAS PROBLEMATICAS','$1',$nrows-1,null,null,null);

commit;

exit; 

EOF
}


#############################
# Main
#############################


cd $INPATH
INFILES=`ls FraudsterList_input_*.csv`
NFILES=`find $INPATH -maxdepth 1 -name "FraudsterList_input_*.csv" | wc -l`

for FILENAME in $INFILES
do

	File_count=`find $INPATH  -maxdepth 1 -name "FraudsterList_input_*.csv" | wc -l`

	if test $File_count -ne 0; 
	then

        in_file=`ls FraudsterList_input_*.csv`
		echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Processing file ${FILENAME}" >> ${LOG}
	
	#Remover ' do ficheiro de input
	sed -i -e 's/'\''/ /g' $in_file

	#Processar ficheiro, load sqlldr + registo Log
	
		load_Moradas_Prob $in_file
		
		insert_customer_details_log $in_file 

		File_count=`find $SQLLDR_PATH -maxdepth 1 -name "sqlldr_$in_file.bad" | wc -l`
		
		if test $File_count -eq 0;
		then
			mv $in_file $ARCHIVEPATH
			gzip $ARCHIVEPATH/$in_file
			echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - Compressing file ${FILENAME} to ${ARCHIVEPATH}" >> ${LOG}
		fi
	fi
done

  
if [ $NFILES -eq 0 ]; then
	echo "$(date +"%Y%m%d%H%M%S")  INFO - ${SCRIPTNAME} - No files found to process" >> ${LOG}
fi

###END#############################################################################################



