CREATE OR REPLACE PROCEDURE DBWR."MORADAS_PROBLEMATICAS" 
AUTHID CURRENT_USER IS
-- declarar variaveis para o cursor
     
    str varchar2(10000);
    num_rows number;
    file_name varchar2(50);
    err_msg varchar2(50);
    contador number:=0;
    
    -- auxiliar cursor
    v_arruamento DBWR.F_CD_CUSTOMER_DETAILS.S_SERVICE_ADDRESS%type;
    v_cp7 DBWR.F_CD_CUSTOMER_DETAILS.S_POSTAL_CODE_INST%type;
    v_data_atua DBWR.F_CD_CUSTOMER_DETAILS.DATA_ACTUALIZACAO_CONTA%type;
    v_contacto_instalacao DBWR.F_CD_CUSTOMER_DETAILS.CONTACTO_TEL%type;
    v_cliente DBWR.F_CD_CUSTOMER_DETAILS.NOME_CLIENTE%type;
    v_email DBWR.F_CD_CUSTOMER_DETAILS.EMAIL%type;
      
    cursor cur_moradas_prob is  
        select data_atua,ARRUAMENTO,substr(CONTACTO_INSTALACAO,0,20), CLIENTE, REPLACE(REPLACE(REPLACE(CP7, chr(10),''),chr(13),''),' '), EMAIL From DBWR.TAB_MORADAS_PROBLEMATICAS;
        
begin

	select count(*) into num_rows from DBWR.F_CD_CUSTOMER_DETAILS where id=0 and NCC='9999999999' and NIC='9999999999' and NIF='999999999';
  
	-- DELETE RECORDS FROM F_CD_CUSTOMER_DETAILS where id=0 and NIF='9999999999' (Old records of TAB_MORADAS_PROBLEMATICAS_NEW)
    str:='DELETE FROM DBWR.F_CD_CUSTOMER_DETAILS where id=0 and NCC=''9999999999'' and NIC=''9999999999'' and NIF=''999999999''';
              
    execute immediate str;      
    commit;
      
    -- Insert num rows delete into Log Table 
    INSERT INTO DBWR.F_CD_CUSTOMER_DETAILS_LOG_T VALUES ('MORADAS PROBLEMATICAS','DELETE',0,'F_CD_CUSTOMER_DETAILS',num_rows,sysdate);
    commit;

	-- INSERT NEW RECORDS of TAB_MORADAS_PROBLEMATICAS_NEW      
    open cur_moradas_prob;    

    loop   
		fetch cur_moradas_prob into v_data_atua,v_arruamento,v_contacto_instalacao,v_cliente,v_cp7,v_email; 
		exit when cur_moradas_prob%notfound;  
   
		begin
             contador:=contador+1;
             str:='INSERT INTO DBWR.F_CD_CUSTOMER_DETAILS (GN_CDR_ID, LAST_MODIFY, TM_TIME_STAMP, ID,NIC, NCC, NUM_FRACCAO, NUM_APARTADO, DATA_ACTIVA_CONTA, DATA_ACTUALIZACAO_CONTA, S_SERVICE_ADDRESS, CONTACTO_TEL, NUMERO_DOC, SIT_CLIENTE_FIXO, ESTADO_DA_CONTA, DATA_FIM_CONTA, NOME_CLIENTE, SEG_VALOR_MOVEL, TIPO_DOC, CORRESP_DEVOL_MOVEL,BDP, S_PHONE_NUMBER, SIT_CLIENTE_MOVEL, NUM_POLICIA, COD_POSTAL_BILLING, CORRESP_DEVOL_FIXO, S_POSTAL_CODE_INST, NIF, TIPO_NIF, MACRO_SEGMENTO, SEG_VALOR_FIXO, SEG_COMPLEMENTAR, EMAIL, UA_BILLING, NOM_ARRUAMENTO, NUM_ANDAR, LOCALIDADE, FONTE_RECOLHA_DADOS) 
             VALUES (fraud_owner.fraudster_man_seq.nextval,sysdate,sysdate,0,''9999999999'',''9999999999'',null,null,null,TO_DATE('''||v_data_atua||''', ''DD-MON-YY''),'''||v_arruamento||''','''||v_contacto_instalacao||''',null,null,null,null,'''||v_cliente||''',null,null,null,null,null,null,null,null,null,'''||v_cp7||''',''999999999'',null,null,null,null,'''||v_email||''',null,null,null,null,null)';
             
			execute immediate str;
            
             commit;
           
             EXCEPTION 
             WHEN OTHERS THEN
			err_msg := SUBSTR(SQLERRM, 1, 40);
			INSERT INTO DBWR.F_CD_CUSTOMER_DETAILS_LOG_T VALUES ('MORADAS PROBLEMATICAS','INSERT,'||err_msg,contador,'F_CD_CUSTOMER_DETAILS',0,sysdate);
            commit;
			
			dbms_output.put_line(str);
			
			RAISE;
		end;                
          
    end loop;

    -- fecha o cursor 
    close cur_moradas_prob;
    
    -- Contador numero registos em TAB_MORADAS_PROBLEMATICAS_NEW apos execucao do script Load_Fraudsters_List.sh (phpfms01)
    select ficheiro,count(*) into file_name, num_rows from DBWR.TAB_MORADAS_PROBLEMATICAS group by FICHEIRO;
    
    -- UPDATE em F_CD_CUSTOMER_DETAILS_LOG_T   
    UPDATE DBWR.F_CD_CUSTOMER_DETAILS_LOG_T
    SET registos_output=num_rows,
        tabela_out='F_CD_CUSTOMER_DETAILS',
        data=sysdate
    WHERE ficheiro_input=file_name;
    
    commit;

    EXCEPTION 
        WHEN OTHERS THEN
        dbms_output.put_line(SQLCODE);
        dbms_output.put_line(SQLERRM);
        RAISE;

end MORADAS_PROBLEMATICAS;
/
