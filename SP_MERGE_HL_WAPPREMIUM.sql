CREATE OR REPLACE PROCEDURE DBWR.SP_MERGE_HL_WAPPREMIUM 
AUTHID CURRENT_USER
IS
stmt varchar2(32000):=null;
errm varchar2(100);
numrows integer;

Begin

INSERT INTO DBWR.TBL_PROCESS_PT_LOG VALUES ('MERGE_HL_WAPPREMIUM',1,'Inicio Merge HotList WapPremium',sysdate);
commit;

stmt:='MERGE INTO fraud_owner.hot_list_data t1
USING (
          SELECT /*+ parallel (t,6) */
              id_originated_number
          FROM
              dbwr.ur_ngin_fut t
          WHERE
              t.tm_start_time between trunc(sysdate - 8,''DD'') and trunc(sysdate,''DD'')
              AND t.nt_operador_virtual NOT IN ( ''50'', ''80'' )
              AND t.gn_record_type = ''SVA_OAPI''
              AND t.nt_serie = ''IPX''
          GROUP BY
              id_originated_number
      )
t2 ON ( t1.value = t2.id_originated_number
        AND t1.hot_list_id = 186 )
WHEN NOT MATCHED THEN
INSERT (
    id,hot_list_id,domain_id,value,description,user_id,time_stamp,last_change,changed_by,created_on,expiry_date )
VALUES
    ( fraud_owner.hl_data_seq.nextval,186,48,t2.id_originated_number,''Subscricao WapPremium'',14,sysdate,sysdate,''14'',sysdate,sysdate + 15 )';

execute immediate stmt;

numrows := SQL%ROWCOUNT;

INSERT INTO DBWR.TBL_PROCESS_PT_LOG VALUES ('MERGE_HL_WAPPREMIUM',2,'Fim Merge HotList WapPremium:'||numrows||' rows inserted',sysdate);
commit;

EXCEPTION 
  WHEN OTHERS THEN
    errm:=substr(SQLERRM,1,100);
    INSERT INTO DBWR.TBL_PROCESS_PT_LOG VALUES ('MERGE_HL_WAPPREMIUM',0,errm,sysdate);
    commit;
    dbms_output.put_line(SQLCODE);
    dbms_output.put_line(SQLERRM);
  RAISE;

END;
/
