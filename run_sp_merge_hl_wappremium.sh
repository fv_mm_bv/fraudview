#!/bin/bash
#
##################################################################################################
#
# NAME
# run_sp_merge_hl_wappremium.sh 
#
# DESCRIPTION
#       - Execucao do procedimento SP_MERGE_HL_WAPPREMIUM
#
# INPUTS
#   	- N/A
#
# OUTPUTS
#   (none)
#
# REMARKS
#   (none)
#
# ------------------------------------------------------------------------------------------
# DATE:        26-05-2022
# AUTHOR:      RMA/MRR- Diamantino Carreira(diamantino-p-carreira@telecom.pt)
# DESCRIPTION:	Execucao do procedimento SP_MERGE_HL_WAPPREMIUM para incluir na hotlist WapPremium 
#				os id_originated_number extraidos da tabela UR_NGIN_FUT.		
#
###################################################################################################
#set -n   # Uncomment to check sintax, without execution.
#set -x   # Uncomment to debug this shell script

#############################
# Global interface
#############################

. /iris/scripts/utils.sh

#############################
# Variaveis Locais
#############################

SCRIPTNAME="run_sp_merge_hl_wappremium"

SN=$(basename $0)                                           # Script name
VER='1.0'                                                   # version

#Script Paths
PATHLOG=${BASELOG}/${SCRIPTNAME}
PATHSPOOL=${BASESPOOL}/${SCRIPTNAME}
PATHSQL=${BASESQL}/${SCRIPTNAME}
LOG=${PATHLOG}/log.txt.${year}-${month}-${day}
sql_query=${PATHSQL}/check_dlu_log_service_orders.sql
result_bd_aux=${PATHSPOOL}/result_db_dlu_log_SO.log.${year}-${month}-${day}


date_log=`date +"%Y%m%d%H%M%S"`
email_suporte=${SUPPORTTEAM}
notify_sms=${SUPPORTTEAMPHONE}
actual_time=`date +"%Y-%m-%d_%H:%M:%S"`

mkdir -p $PATHSPOOL
mkdir -p $PATHLOG
mkdir -p $PATHSQL

exec 1>>$LOG
exec 2>&1

###############
#Functions
###############

envia_email(){
	echo "$actual_time - $1 - ERRO - A enviar email para $email_suporte... " >> $LOG

/usr/sbin/sendmail $email_suporte << END
Subject: FraudView - Alarme - $SCRIPTNAME - $date_log
from:$SUPPORTTEAM
to:$email_suporte

$message

$2


Obrigado,

$SUPPORTTEAMNAME
$SUPPORTTEAM
END

}

#####################
#	MAIN	        #
#####################

run_proc=`sqlplus -S <<EOF
$ORA_LOGON
set timing on
begin
SP_MERGE_HL_WAPPREMIUM();
end;
/
EOF`

if [ `echo ${run_proc} | egrep -c '(ERROR|SP2-[0-9]+:|ORA-[0-9]+:|O/S Message)'` -ne 0 ]; then
	echo "$actual_time - $SCRIPTNAME - Erro ao correr procedimento SP_MERGE_HL_WAPPREMIUM na BD" >>$LOG
	envia_email "Erro SQL na execução do procedimento SP_MERGE_HL_WAPPREMIUM()."
fi

echo "$actual_time - $SCRIPTNAME - Execucao do procedimento SP_MERGE_HL_WAPPREMIUM|${year}-${month}-${day}| com sucesso" >> $LOG
	
exit
